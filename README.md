# README #

### Winter Wonderland ###

* This project is a website for Winter Wonderland.

### How do I get set up? ###

* This is an HTML, CSS project
* Clone the project and open the html file, the website will be running on the web browser. 

### Who do I talk to? ###

* Repo owner: Akhila Ann Mariya

# Note: This project is licensed under the terms of the MIT license. 
* MIT License is short and to the point. Since this project is a basic website, it only needs preservation of copyright.